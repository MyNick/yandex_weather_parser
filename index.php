<?php

require_once __DIR__.'/bootstrap.php';

require_once __DIR__ . '/twig/vendor/autoload.php';

$loader = new Twig_Loader_Filesystem(__DIR__ . '/twig/views');
$twig = new Twig_Environment($loader, ['cache' => __DIR__ . '/twig/cache']);

use Models\Day;
use Models\Weather;
use Models\Precipitation;
use Models\Times_of_day;


require_once 'simple_html_dom.php';

# Parse data and save to database
if (isset($_POST['send'])) {

Weather::truncate();
Day::truncate();

$html = file_get_html("https://yandex.ru/pogoda/moscow/details");
$n = 1;

for ($i = 0; $i < 8; $i++) {

  $d=$html->find("dt.forecast-details__day")[$i];

  $day = new Day;
  $day->name = $d->find("strong.forecast-details__day-number")[0]->plaintext.' '.$d->find("span.forecast-details__day-month")[0]->plaintext.', '.$d->find("span.forecast-details__day-name")[0]->plaintext;
  if(!$day->save()) die('["Ошибка запроса 1.<br>"]');#echo "last id = ".$day->id;

    for ($n = 0; $n < 4; $n++) {$s = $html->find(".weather-table__body")[$i];
    	$weather = new Weather;
    	$weather->temperature = $s->find(".weather-table__temp",$n)->plaintext;
    	$weather->pressure = $s->find("td.[class=weather-table__body-cell weather-table__body-cell_type_air-pressure]",$n)->plaintext;
    	$weather->humidity = $s->find("td.[class=weather-table__body-cell weather-table__body-cell_type_humidity]",$n)->plaintext;
    	$weather->wind = $s->find("span.wind-speed",$n)->plaintext.' '.$s->find(".weather-table__wind-direction",$n)->plaintext;
    	$weather->feels_like = $s->find("td.[class=weather-table__body-cell weather-table__body-cell_type_feels-like] span.[class=temp__value]",$n)->plaintext;
    	
    	$g = $s->find("td.[class=weather-table__body-cell weather-table__body-cell_type_condition]",$n)->plaintext;
    	$weather->precipitation_id = Precipitation::where('name', $g)->first()->id;
    	
    	$daypart = $s->find(".weather-table__daypart",$n)->plaintext;
    	$daypart = mb_strtoupper(mb_substr($daypart, 0, 1)).mb_substr($daypart, 1);#Simple multi-bytes ucfirst()
    	$weather->times_of_day_id = Times_of_day::where('name', $daypart)->first()->id;
    	$weather->day_id = $day->id;

    	if (!$weather->save()) die('["Ошибка запроса 2.<br>"]');
    }
}
die('["Данные сохранены."]');#JSON
}

$q = Weather::with('day', 'precipitation', 'times_of_day')->groupBy('day_id')->get();

function getDay($day_id) {
  return $q2 = Weather::with('day', 'precipitation', 'times_of_day')->where('day_id', '=', $day_id)->get();
}

$function = new Twig_Function('getDay', function ($day_id) {
  return $q2 = Weather::with('day', 'precipitation', 'times_of_day')->where('day_id', '=', $day_id)->get();
});

$twig->addFunction($function);

echo $twig->render('db.twig', ['q' => $q, 'q2' => '$q2']);
