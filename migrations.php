<?php

require_once __DIR__.'/bootstrap.php';

use Illuminate\Database\Schema\Blueprint;

$obj = new Weather($capsule);
$obj->up();#$obj->down();

class Weather
{
    public function __construct($capsule = null) { $this->capsule = $capsule; }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $this->capsule::schema()->create('day', function (Blueprint $table) {
            $table->smallIncrements('id'); #UNSIGNED SMALL INTEGER
            $table->string('name', 99)->unique(); #VARCHAR with length

            #$table->string('ufindex', 99);

            #$table->string('daylight', 99);

            #$table->integer('moon_phase_id')->unsigned();
            #$table->foreign('moon_phase_id')->references('id')->on('moon_phase');

            #$table->integer('magnetic_field_id')->unsigned();
            #$table->foreign('magnetic_field_id')->references('id')->on('magnetic_field');

        });

        $this->capsule::schema()->create('times_of_day', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('name', 99)->unique();
        });

        $this->capsule::schema()->create('precipitation', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('name', 99)->unique();
        });

        $this->capsule::schema()->create('weather', function (Blueprint $table) {
            $table->smallIncrements('id'); # UNSIGNED SMALL INTEGER
            $table->string('temperature', 255);
            $table->integer('pressure');            
            $table->integer('humidity');
            $table->string('wind', 255);
            $table->string('feels_like', 255);

            $table->integer('precipitation_id')->unsigned();
            $table->foreign('precipitation_id')->references('id')->on('precipitation');
            $table->integer('day_id')->unsigned();
            $table->foreign('day_id')->references('id')->on('day');
            $table->integer('times_of_day_id')->unsigned();
            $table->foreign('times_of_day_id')->references('id')->on('times_of_day');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->capsule::schema()->dropIfExists('weather');
        $this->capsule::schema()->dropIfExists('day');
        $this->capsule::schema()->dropIfExists('precipitation');
        $this->capsule::schema()->dropIfExists('times_of_day');
    }
}
