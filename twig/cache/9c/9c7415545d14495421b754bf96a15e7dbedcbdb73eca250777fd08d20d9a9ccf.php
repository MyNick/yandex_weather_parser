<?php

/* db.twig */
class __TwigTemplate_c102be2bb7708d09fb7e8e0d3acda4794b6c228f858974a487125e611e8a7bdd extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
  <head>
\t<meta charset=\"utf-8\" />
\t<title>Прогноз погоды в Москве на 8 дней — Яндекс.Погода</title>
\t<link href=\"includes/style.css\" rel=\"stylesheet\">
\t<link href=\"includes/yandex.css\" rel=\"stylesheet\">
  </head>
  <body><br>

  <form action=\"index.php\" method=\"post\" id=\"form\" name=\"form\">
    <p class=\"center\"><input type=\"submit\" name=\"send\" class=\"button\" id=\"get_data\" value=\"Спарсить данные в БД\"><span id=\"error_span\"></span></p>
  </form>

  <h2>Прогноз на 8 дней</h2><strong>Погода в Москве</strong><br><br><br>

";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["q"] ?? null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 17
            echo "
<dt class=\"forecast-details__day\" data-anchor=\"";
            // line 18
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "id", array()), "html", null, true);
            echo "\">
  <strong class=\"forecast-details__day-number\">";
            // line 19
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "name", array()), "html", null, true);
            echo "</strong>
</dt>

\t<dd class=\"forecast-details__day-info\"><table class=\"weather-table\" _border=1><thead class=\"weather-table__head\"><th class=\"weather-table__head-cell\" colspan=\"3\">";
            // line 22
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["r"], "day", array()), "name", array()), "html", null, true);
            echo "</th><th class=\"weather-table__head-cell\"><U-LO8Q class=\"weather-table__value\">Давление, <br>мм рт. ст.</U-LO8Q></th><th class=\"weather-table__head-cell\"><U-LO8Q class=\"weather-table__value\">Влажность</U-LO8Q></th><th class=\"weather-table__head-cell weather-table__head-cell_type_wind\"><U-LO8Q class=\"weather-table__value\">Ветер, м/с</U-LO8Q></th><th class=\"weather-table__head-cell weather-table__head-cell_type_feels-like\"><U-LO8Q class=\"weather-table__value\">Ощущается как</U-LO8Q></th></thead>

<tbody class=\"weather-table__body\">
";
            // line 25
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(call_user_func_array($this->env->getFunction('getDay')->getCallable(), array(twig_get_attribute($this->env, $this->source, $context["r"], "day_id", array()))));
            foreach ($context['_seq'] as $context["_key"] => $context["r2"]) {
                // line 26
                echo "\t<tr class=\"weather-table__row\">
\t\t<td class=\"weather-table__body-cell weather-table__body-cell_type_daypart weather-table__body-cell_wrapper\">
\t\t\t<U-LO8Q class=\"weather-table__wrapper\">
\t\t\t\t<U-LO8Q class=\"weather-table__daypart\">";
                // line 29
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["r2"], "times_of_day", array()), "name", array()), "html", null, true);
                echo "</U-LO8Q>
\t\t\t\t<U-LO8Q class=\"weather-table__temp\">
\t\t\t\t\t";
                // line 31
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r2"], "temperature", array()), "html", null, true);
                echo "
\t\t\t\t</U-LO8Q>
\t\t\t</U-LO8Q>
\t\t</td>
\t\t<td class=\"weather-table__body-cell weather-table__body-cell_type_icon\">
\t\t\t<img class=\"icon icon_thumb_bkn-m-sn-n icon_color_dark\" src=\"//yandex.ru/pogoda/...\" aria-hidden=\"true\"/>
\t\t</td>
\t\t<td class=\"weather-table__body-cell weather-table__body-cell_type_condition\">";
                // line 38
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["r2"], "precipitation", array()), "name", array()), "html", null, true);
                echo "</td>
\t\t<td class=\"weather-table__body-cell weather-table__body-cell_type_air-pressure\">";
                // line 39
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r2"], "pressure", array()), "html", null, true);
                echo "</td>
\t\t<td class=\"weather-table__body-cell weather-table__body-cell_type_humidity\">";
                // line 40
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r2"], "humidity", array()), "html", null, true);
                echo "</td>
\t\t<td class=\"weather-table__body-cell weather-table__body-cell_type_wind weather-table__body-cell_wrapper\">";
                // line 41
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r2"], "wind", array()), "html", null, true);
                echo "
\t\t</td>
\t\t<td class=\"weather-table__body-cell weather-table__body-cell_type_feels-like\"><U-LO8Q class=\"temp\"><span class=\"temp__value\">";
                // line 43
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r2"], "feels_like", array()), "html", null, true);
                echo "</span><span class=\"temp__unit i-font i-font_face_yandex-sans-text-medium\">°</span></U-LO8Q></td>
\t</tr>
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r2'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 46
            echo "
</tbody></table></dd>";
            // line 47
            if ((twig_get_attribute($this->env, $this->source, $context["loop"], "index", array()) == 1)) {
                echo "<br><br><br>";
            }
            echo "<br>

";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "
  <script src=\"includes/js/jquery-3.3.1.min.js\"></script>
  <script src=\"includes/js/jquery.form.min.js\"></script>
  <script src=\"includes/js/form.js\"></script>

  </body>
</html>";
    }

    public function getTemplateName()
    {
        return "db.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  148 => 50,  129 => 47,  126 => 46,  117 => 43,  112 => 41,  108 => 40,  104 => 39,  100 => 38,  90 => 31,  85 => 29,  80 => 26,  76 => 25,  70 => 22,  64 => 19,  60 => 18,  57 => 17,  40 => 16,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "db.twig", "D:\\www\\test\\CapsuleTest\\twig\\views\\db.twig");
    }
}
