<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
    protected $table = 'day';
    public $timestamps = false;

    public function weather()
    {
        return $this->hasMany('Models\Weather');
    }
}
