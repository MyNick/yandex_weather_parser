<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class Weather extends Model
{
    protected $table = 'weather';
    public $timestamps = false;

    public function day()
    {
        return $this->belongsTo('Models\Day');
    }
    public function precipitation()
    {
    	return $this->belongsTo('Models\Precipitation');
    }
    public function Times_of_day()
    {
        return $this->belongsTo('Models\Times_of_day');
    }
}
