<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class Times_of_day extends Model
{
    protected $table = 'times_of_day';
    public $timestamps = false;

    public function weather()
    {
        return $this->hasOne('Models\Weather');
    }
}
