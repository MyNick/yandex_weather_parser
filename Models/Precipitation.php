<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class Precipitation extends Model
{
    protected $table = 'precipitation';
    public $timestamps = false;

    public function weather()
    {
        return $this->hasMany('Models\Weather');
    }
}
